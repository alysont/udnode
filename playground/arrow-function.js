const square = x => x * x;
console.log(square(9));

const user = {
  name: 'Andrew',
  sayHi: () => {
    // it doesn't work
    // console.log(arguments);
    console.log(`Hi I'm ${this.name}`);
  },
  sayHiAlt() {
    console.log(arguments);
    console.log(`Hi I'm ${this.name}`);
  },
};

user.sayHiAlt(1, 2, 3);
